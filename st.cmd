require essioc

epicsEnvSet("ENGINEER", "Gabriel Fedel <gabriel.fedel@ess.eu>")
## Add extra environment variables here

epicsEnvSet("TOP",      "$(E3_CMD_TOP)")

# Load standard module startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

require mrfioc2, 2.3.1-beta.5
require evrisland, dev
iocshLoad("$(evrisland_DIR)/island_with_mrfioc_evm.iocsh")
dbl > PVs.list

# Call iocInit to start the IOC
iocInit()
date
